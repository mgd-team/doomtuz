window.addEventListener('votingSave', function (event) {
    if ('votingResult' !== ifaceMode) {
        return;
    }
    const {detail: {votingResult, questionText}} = event;
    var result = JSON.parse(votingResult)
    const jsSession = document.getElementsByClassName("js-session");
    let jsQuestions = document.getElementsByClassName("js-questions");
    const addVotingResult = (question, keyResult, append) => {

        let voting = $(question).find('.js-voting'),
            tableResult = $(question).find('.js-tableResult'),
            votingElement = $(voting[0]),
            tableResultElement = $(tableResult[0]),
            index = append?voting.length + 1:1
        if (append) {
            votingElement = votingElement.clone()
            tableResultElement = tableResultElement.clone()
        }
        $(votingElement.find('a')[0]).attr('data-index', index).html('<p>' + votingLabels + index + '</p>')
        let htmlTitle = (votingResult, votingD, amount) => {
            if (votingResult) {
                return '<a class="jsVotesResult"' +
                    'data-questions-id="' + keyResult + '"' +
                    'data-group="' + (voting.length + 1) + '"' +
                    'data-voting=' + votingD +
                    ' href="javascript:void(0)">' + amount + '</a>'
            }
            else {
                return amount
            }
        }
        let agree = 0,
            disagree = 0,
            ignore = 0,
            i = 0

        for (let keyVoting in result[keyResult]) {
            if (1 == result[keyResult][keyVoting].result) {
                ++agree;
            }
            if (2 == result[keyResult][keyVoting].result) {
                ++disagree;
            }
            if (3 == result[keyResult][keyVoting].result) {
                ++ignore;
            }
            i++
        }
        let neutral = countRegistrationClient - i
        $(tableResultElement.find('tr.js-agree .title')[0]).html(htmlTitle(agree, 'agree', amountOfAgree))
        $(tableResultElement.find('tr.js-agree td')[0]).html(agree)
        $(tableResultElement.find('tr.js-disagree .title')[0]).html(htmlTitle(disagree, 'disagree', amountOfDisagree))
        $(tableResultElement.find('tr.js-disagree td')[0]).html(disagree)
        $(tableResultElement.find('tr.js-ignore .title')[0]).html(htmlTitle(ignore, 'ignore', amountOfIgnore))
        $(tableResultElement.find('tr.js-ignore td')[0]).html(ignore)
        $(tableResultElement.find('tr.js-neutral .title')[0]).html(htmlTitle(neutral, 'neutral', amountOfNeutral))
        $(tableResultElement.find('tr.js-neutral td')[0]).html(neutral)
        if (append) {
            $(question).append(votingElement)
            $(question).append(tableResultElement)
        }
    }
    let titleQuestions = document.getElementsByClassName('js-questions'),
        arrQuestioonIds = []
    for (let keyTitleQuestions in titleQuestions){
        if (titleQuestions[keyTitleQuestions].dataset) {
            arrQuestioonIds.push(titleQuestions[keyTitleQuestions].dataset.questionId)
        }
    }
    for (let keyResult in result) {
        for (let questionsKey in jsQuestions) {
            if (jsQuestions[questionsKey].dataset) {
                if (jsQuestions[questionsKey].dataset.questionId === keyResult) {
                    addVotingResult(jsQuestions[questionsKey], keyResult, append = true)
                } else if (arrQuestioonIds.indexOf( keyResult ) == -1) {
                    let question =  $(jsQuestions[0]).clone().attr('data-question-id', keyResult),
                        voting = $(question.find('.js-voting')[0]).clone(),
                        tableResult = $(question.find('.js-tableResult')[0]).clone(),
                        titleQuestion = $(question.find('.js-titleQuestion')[0]).clone().html('<p class="text-left">'+questionText+'</p>')
                    $(question).html(titleQuestion)
                    $(question).append(voting)
                    $(question).append(tableResult)
                    let session = $(jsSession).append(question)
                    jsQuestions = session.find(".js-questions");
                    addVotingResult(jsQuestions[jsQuestions.length - 1], keyResult, append = false)
                    break;
                }
            }
        }

    }
})

