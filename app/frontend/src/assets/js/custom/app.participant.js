// Participant list sort
const ONLINE = 'status_online';
const OFFLINE = 'status_offline';

const ONLINE_SORT_VALUE = 1;
const OFFLINE_SORT_VALUE = 2;
const DEFAULT_SORT_VALUE = 3;

const $users = $('#participant-list').children();

const getSortValue = function (status) {
    switch (status) {
        case ONLINE:
            return ONLINE_SORT_VALUE;
        case OFFLINE:
            return OFFLINE_SORT_VALUE;
        default:
            return DEFAULT_SORT_VALUE;
    }
};

const sortUsers = function (user1, user2) {
    let user1Id = $(user1).data('userId')
    let user2Id = $(user2).data('userId')
    const speechUser1 = $('#speech-' + user1Id).children().data('timeString') ? $('#speech-' + user1Id).children().data('timeString') : false;
    const managerUser1 = $('#manager-' + user1Id).children().data('timeString') ? $('#manager-' + user1Id).children().data('timeString') : false;
    const speechUser2 = $('#speech-' + user2Id).children().data('timeString') ? $('#speech-' + user2Id).children().data('timeString') : false;
    const managerUser2 = $('#manager-' + user2Id).children().data('timeString') ? $('#manager-' + user2Id).children().data('timeString') : false;
    const statusUser1 = getSortValue($(user1).attr('data-status'));
    const statusUser2 = getSortValue($(user2).attr('data-status'));
    if (managerUser1 && managerUser2) {
        return sortUsersResult(managerUser1, managerUser2);
    } else if ( managerUser1) {
        return -1;
    } else if (managerUser2 ) {
        return 1;
    } else if (speechUser1 && speechUser2) {
        return sortUsersResult(speechUser1, speechUser2);
    } else if (speechUser1 ) {
        return -1;
    } else if (speechUser2 ) {
        return 1;
    } else if (statusUser1 === statusUser2) {
        const nameUser1 = $(user1).find('span.kt-widget__username').text();
        const nameUser2 = $(user2).find('span.kt-widget__username').text();

        return nameUser1.localeCompare(nameUser2, 'ru');
    }
    else {
        return sortUsersResult(statusUser1, statusUser2);
    }
}
;

const sortSpeechesForTime = function (user1, user2) {
    const statusUser1 = $('#speech-' + $(user1).data('userId')).children().data('timeString');
    const statusUser2 = $('#speech-' + $(user2).data('userId')).children().data('timeString');
    ;
    return sortUsersResult(statusUser1, statusUser2)
};

const sortManagersForTime = function (user1, user2) {
    const statusUser1 = $('#manager-' + $(user1).data('userId')).children().data('timeString');
    ;
    const statusUser2 = $('#manager-' + $(user2).data('userId')).children().data('timeString');
    return sortUsersResult(statusUser1, statusUser2)
};

const sortUsersResult = (user1, user2) => {
    return (user1 < user2) ? -1 : (user1 > user2) ? 1 : 0;
}

const usersSort = (sortFunction) => {
    $users.sort(sortFunction).appendTo('#participant-list');
}

const changeLabel = function (user, status) {
    const $statusSpan = $(user).find('.participant_item_status');
    $statusSpan.removeClass((index, className) => {
        return (className.match(/(^|\s)status_\S+/g) || []).join(' ');
    });
    $statusSpan.addClass(status);
    $statusSpan.empty().text(Translator.trans('statuses.' + status));
};

const changeUserStatus = function (users, usersIds, status = ONLINE) {
    $(users).each((index, user) => {
        const userId = parseInt($(user).attr('data-user-id'));
        if (usersIds.includes(userId)) {
            changeLabel(user, status);
            $(user).attr('data-status', status);
        }
    })
};

// Drag and drop initialization
document.querySelectorAll('.participant_item').forEach(participant => {
    participant.ondragstart = (event) => {
        event.dataTransfer.setData("text/plain", participant.getAttribute('data-user-id'));
        document.querySelectorAll('.drop-zone').forEach(dropZone => {
            dropZone.classList.remove('kt-hidden');
        });
    };
    participant.ondragend = () => {
        document.querySelectorAll('.drop-zone').forEach(dropZone => {
            dropZone.classList.add('kt-hidden');
        });
        // const eventDelete = (eventName) => {
        //     const event = new CustomEvent(eventName + 'Delete', {
        //         'detail': {
        //             'sender': participant.dataset.userId,
        //             'delete': true
        //         }
        //     });
        //     window.dispatchEvent(event);
        // }
        // if (document.getElementById('manager-' + participant.dataset.userId).getElementsByTagName('span').length) {
        //     eventDelete('manager')
        // }
        // if (document.getElementById('speech-' + participant.dataset.userId).getElementsByTagName('span').length) {
        //     eventDelete('speech')
        // }
    };
});

// Participants filter
window.addEventListener("load", function () {

    const ROLE_ADMIN = 4;
    const ROLE_MEMBER = 1;
    const ROLE_DEPUTY = 2;

    const SPEECH = 10;
    const MANAGER = 11;

    const search = document.getElementById("searchMember");
    if (null === search) {
        return;
    }

    const enableRoles = [];
    const registrationFiltr = [];
    const membersArr = document.querySelectorAll(".participant_item");
    const adminCheckBox = document.getElementById("adminCheck"); // 4
    const memberCheckBox = document.getElementById("memberCheck"); // 1
    const deputyCheckBox = document.getElementById("deputyCheck"); // 2
    const speechCheckBox = document.getElementById("speechCheck");
    const managerCheckBox = document.getElementById("managerCheck");

    let searchValue = "";

    search.oninput = () => {
        filterByMembers();
        searchValue = search.value.toLowerCase();
    };

    adminCheckBox.onclick = () => {
        if (adminCheckBox.checked) {
            enableRoles.push(ROLE_ADMIN);
        } else {
            enableRoles.splice(enableRoles.indexOf(ROLE_ADMIN), 1);
        }
        filterByMembers();
    };

    memberCheckBox.onclick = () => {
        if (memberCheckBox.checked) {
            enableRoles.push(ROLE_MEMBER);
        } else {
            enableRoles.splice(enableRoles.indexOf(ROLE_MEMBER), 1);
        }
        filterByMembers();
    };

    deputyCheckBox.onclick = () => {
        if (deputyCheckBox.checked) {
            enableRoles.push(ROLE_DEPUTY);
        } else {
            enableRoles.splice(enableRoles.indexOf(ROLE_DEPUTY), 1);
        }
        filterByMembers();
    };

    speechCheckBox.onclick = () => {
        if (speechCheckBox.checked) {
            registrationFiltr.push(SPEECH);
        } else {
            registrationFiltr.splice(registrationFiltr.indexOf(SPEECH), 1);
        }
        filterByMembers();
        usersSort(sortSpeechesForTime);
    };
    managerCheckBox.onclick = () => {
        if (managerCheckBox.checked) {
            registrationFiltr.push(MANAGER);
        } else {
            registrationFiltr.splice(registrationFiltr.indexOf(MANAGER), 1);
        }
        filterByMembers();
        usersSort(sortManagersForTime);
    };

    const filterByMembers = () => {
        for (let i = 0; i < membersArr.length; i++) {
            const role = Number(membersArr[i].getAttribute("data-role"));
            const registration = []
            if (document.getElementById('speech-' + membersArr[i].dataset.userId).getElementsByTagName('span').length) {
                registration.push(SPEECH)
            }
            ;
            if (document.getElementById('manager-' + membersArr[i].dataset.userId).getElementsByTagName('span').length) {
                registration.push(MANAGER)
            }
            ;
            const nameMatched = !search.value || (search.value && membersArr[i]
                        .getAttribute("data-name")
                        .toLowerCase()
                        .includes(search.value.toLowerCase())
                );
            const roleMatched = 0 === enableRoles.length || enableRoles.indexOf(role) !== -1;
            var registrationMatched = 0 === registrationFiltr.length || false;
            registration.map((item) => {
                if (!registrationMatched) {
                    registrationMatched = registrationFiltr.indexOf(item) !== -1;
                }
            })

            if (nameMatched && roleMatched && registrationMatched) {
                membersArr[i].style.display = "block";
            } else {
                membersArr[i].style.display = "none";
            }
        }
    };
});


window.addEventListener('onParticipantConnect', function (event) {

    let roomId = $('#participant-list').attr('data-room-id');
    const {detail: {connected_users: onlineUsers, registered_users: registeredUsers}} = event;
    let registered = [];
    let online = [];
    $.each(registeredUsers, (index, users) => {
        if (index == roomId) {
            registered = users;
        }
    });
    $.each(onlineUsers, (index, users) => {
        if (index == roomId) {
            online = users;
        }
    })
    if ($('#dateModalByListRegistered')) {
        $.each(registeredUsers, (index, users) => {
            $.each(users, (index, user) => {
                    $("#userRegistration-"+user).show().addClass('add')

            })
        })
        let userRegistration = $('.userRegistration.add')
        $('#registrationLength').html(userRegistration.length)
    }
    changeUserStatus($users, registered, OFFLINE);
    changeUserStatus($users, online, ONLINE);
    usersSort(sortUsers);
    let userOnline = $(".participant_item[data-status = 'status_online']")
    let userOffline = $(".participant_item[data-status = 'status_offline']")
    let userAdd = userOnline.add(userOffline)
    $("#countParticipant").html(userAdd.length)
});
