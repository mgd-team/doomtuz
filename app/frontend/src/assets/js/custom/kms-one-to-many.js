/*
 * (C) Copyright 2014-2015 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const KmsOneToMany = function (
    appLocation,
    video,
    onReady,
    stunServer
) {
    let webRtcPeer;
    const listeners = {
        onPresenterAccepted: null,
        onViewerAccepted: null,
        onError: null,
        onDispose: null,
    };

    window.onbeforeunload = function() {
        ws.close();
    };

    appLocation = new URL(appLocation);
    const ws = io.connect(appLocation.host, {
        path: appLocation.pathname,
        secure: true,
        transports: ["websocket"],
        upgrade: false
    });
    ws.on("connect", onReady);

    ws.on("error", onError);

    ws.on("disconnect", (reason) => {
        console.info(`Connection closed`, reason);
    });

    ws.on("message", function(message) {
        const parsedMessage = JSON.parse(message);
        console.info('Received message: ' + message);

        switch (parsedMessage.id) {
            case 'presenterResponse':
                presenterResponse(parsedMessage);
                break;
            case 'viewerResponse':
                viewerResponse(parsedMessage);
                break;
            case 'stopCommunication':
                dispose();
                break;
            case 'iceCandidate':
                webRtcPeer.addIceCandidate(parsedMessage.candidate);
                break;
            default:
                console.error('Unrecognized message', parsedMessage);
        }
    });

    function onError(error) {
        console.error(error);
        if (null !== listeners.onError) {
            listeners.onError();
        }
    }

    function presenterResponse(message) {
        if (message.response !== 'accepted') {
            const errorMsg = message.message ? message.message : 'Unknow error';
            console.warn('Call not accepted for the following reason: ' + errorMsg);
            dispose();
        } else {
            console.info('Presenter accepted!');
            if (null !== listeners.onPresenterAccepted) {
                listeners.onPresenterAccepted();
            }
            webRtcPeer.processAnswer(message.sdpAnswer);
        }
    }

    function viewerResponse(message) {
        if (message.response !== 'accepted') {
            const errorMsg = message.message ? message.message : 'Unknow error';
            console.warn('Call not accepted for the following reason: ' + errorMsg);
            dispose();
        } else {
            console.info('Viewer accepted!');
            if (null !== listeners.onViewerAccepted) {
                listeners.onViewerAccepted();
            }
            webRtcPeer.processAnswer(message.sdpAnswer, () => {
                console.info("Process answer!");
            });
        }
    }

    function presenter() {
        if (!webRtcPeer) {

            const options = {
                localVideo: video,
                onicecandidate : onIceCandidate,
                configuration: {
                    iceServers: [{"urls":`stun:${stunServer}`}]
                },
            };

            webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
                if(error) return onError(error);

                this.generateOffer(onOfferPresenter);
            });
        }
    }

    function onOfferPresenter(error, offerSdp) {
        if (error) return onError(error);

        const message = {
            id : 'presenter',
            sdpOffer : offerSdp
        };
        sendMessage(message);
    }

    function viewer() {
        if (!webRtcPeer) {

            const options = {
                remoteVideo: video,
                onicecandidate : onIceCandidate,
                configuration: {
                    iceServers: [{"urls":`stun:${stunServer}`}]
                },
            };

            webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
                if(error) return onError(error);

                this.generateOffer(onOfferViewer);
            });
        }
    }

    function onOfferViewer(error, offerSdp) {
        if (error) return onError(error);

        const message = {
            id : 'viewer',
            sdpOffer : offerSdp
        };
        sendMessage(message);
    }

    function onIceCandidate(candidate) {
        console.log('Local candidate' + JSON.stringify(candidate));

        const message = {
            id : 'onIceCandidate',
            candidate : candidate
        };
        sendMessage(message);
    }

    function stop() {
        if (webRtcPeer) {
            const message = {
                id : 'stop'
            };
            sendMessage(message);
            dispose();
        }

        if (ws.readyState === ws.OPEN) {
            ws.close();
        }
    }

    function dispose() {
        if (webRtcPeer) {
            webRtcPeer.dispose();
            webRtcPeer = null;
            if (null !== listeners.onDispose) {
                listeners.onDispose();
            }
        }
    }

    function sendMessage(message) {
        const jsonMessage = JSON.stringify(message);
        console.log('Sending message: ' + jsonMessage);
        ws.emit("message", jsonMessage);
    }

    return {
        stop,
        viewer,
        presenter,
        listeners,
    };
};
