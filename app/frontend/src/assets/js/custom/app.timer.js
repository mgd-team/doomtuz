
const minutesLabel = document.getElementById("minutes");
const secondsLabel = document.getElementById("seconds");
let timerInterval, totalSeconds = 0;

function startTimer(timer_sec = 0) {
    clearInterval(timerInterval);
    totalSeconds = timer_sec;
    timerInterval = setInterval(setTime, 1000);
}

function stopTimer()
{
    clearInterval(timerInterval);
}

function setTime() {
    ++totalSeconds;
    secondsLabel.innerHTML = pad(totalSeconds % 60);
    minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
}

function pad(val) {
    let valString = val + "";
    if (valString.length < 2) {
        return "0" + valString;
    } else {
        return valString;
    }
}

window.addEventListener('onStreamStarted', function (event) {
    const { detail: { target: target } } = event;
    if ('tribune' === target || 'member' === target) {
        startTimer();
    }
});

window.addEventListener('onStreamStopped', function (event) {
    const { detail: { target: target } } = event;
    if ('tribune' === target || 'member' === target) {
        stopTimer();
    }
});
