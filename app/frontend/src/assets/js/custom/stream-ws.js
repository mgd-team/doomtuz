
function StreamSocket (
    socketAddress,
    id,
    tribuneKMSAppLocation,
    memberKMSAppLocation,
    presidiumKMSAppLocation,
    tableKMSAppLocation,
    stunServer
) {
    const USER_ID = id;

    let SOCKET;

    let kmsApps = {
        'tribune': {
            'location': `${tribuneKMSAppLocation}`,
            'video': document.getElementById('tribune-video'),
            'app': null,
            stunServer,
        },
        'member': {
            'location': `${memberKMSAppLocation}`,
            'video': document.getElementById('member-video'),
            'app': null,
            stunServer,
        },
        'presidium': {
            'location': `${presidiumKMSAppLocation}`,
            'video': document.getElementById('presidium-video'),
            'app': null,
            stunServer,
        },
        'table': {
            'location': `${tableKMSAppLocation}`,
            'video': document.getElementById('table-video'),
            'app': null,
            stunServer,
        },
    };

    function connectToServer() {
        SOCKET = io.connect(socketAddress, {
            secure: true,
            transports: ["websocket"],
            upgrade: false
        });

        addSocketListeners();
        addEventListeners();
    }

    function addSocketListeners() {

        SOCKET.on("connect", function() {
            SOCKET.on('ready', function (activeTargets) {
                // Проверяем все активные плееры
                for (let target in activeTargets) {
                    // Если текущий пользователь должен быть активным, то стартуем активные потоки
                    if (USER_ID === activeTargets[target].userId) {
                        SOCKET.emit('onStartStream', activeTargets[target]);
                    } else {
                        startViewer(target, activeTargets[target].muted);
                    }
                    emitPlayerInitialized(target, activeTargets[target]);
                }
            });

            // Сообщаем на сокет-сервер свой ID
            SOCKET.emit('init', {userId: USER_ID});
        });

        SOCKET.on("onStartStream", function(event) {
            if (USER_ID === event.userId) {
                startPresenter(event.target, event.muted, function () {
                    SOCKET.emit('onStreamStarted', event);
                });
            } else {
                stop(event.target);
            }
        });

        SOCKET.on("onStreamStarted", function(event) {
            if (USER_ID !== event.userId) {
                startViewer(event.target, event.muted)
            }

            emitStreamStarted(event.target, event.userId);
        });

        SOCKET.on("onStopStream", function(event) {
            if (USER_ID === event.userId) {
                SOCKET.emit('onStreamStopped', event);
            }
        });

        SOCKET.on("onStreamStopped", function(event) {
            stop(event.target);
            emitStreamStopped(event.target, event.userId);
        });

        SOCKET.on("onMuteStream", function(event) {
            if (USER_ID !== event.userId) {
                mute(event.target);
            }

            const e = new CustomEvent('onMuteStream', {'detail': event});
            window.dispatchEvent(e);
        });

        SOCKET.on("onUnmuteStream", function(event) {
            if (USER_ID !== event.userId) {
                unmute(event.target);
            }

            const e = new CustomEvent('onUnmuteStream', {'detail': event});
            window.dispatchEvent(e);
        });

        SOCKET.on("connection_error", function() {
            log("Connection failed");
        });

        SOCKET.on("stream_error", e => {
            log(`Stream ${e.streamId} error`, e);
        });

        SOCKET.on("disconnect", function() {
            log("ERROR: server disconnected!");
        });
    }

    function addEventListeners() {
        window.addEventListener('emit_onStopStream', function (event) {
            const { detail: { target: target } } = event;
            SOCKET.emit('onStopStream', {target});
        });

        window.addEventListener('emit_onStartStream', function (event) {
            const { detail: { userId: userId, target: target } } = event;
            SOCKET.emit('onStartStream', {target, userId});
        });

        window.addEventListener('emit_onMuteStream', function (event) {
            const { detail: { target: target } } = event;
            SOCKET.emit('onMuteStream', {target});
        });

        window.addEventListener('emit_onUnmuteStream', function (event) {
            const { detail: { target: target } } = event;
            SOCKET.emit('onUnmuteStream', {target});
        });
    }

    function log(msg, ...params) {
        console.log(`Stream socket: ${msg}`, ...params);
    }

    function startPresenter (player, muted, callback) {
        if (kmsApps[player] && null !== kmsApps[player].app) {
            stop(player);
        }

        console.log('Presenter start on player: '+player);

        emitStartStreamLoading(player);

        try {
            kmsApps[player].app = new KmsOneToMany(
                kmsApps[player].location,
                kmsApps[player].video,
                function () {
                    console.log('Presenter ready on player: '+player+' (muted: '+muted+')');

                    emitSelfStreamStarted(player);

                    kmsApps[player].app.listeners.onPresenterAccepted = () => {
                        console.log('Presenter accepted on player: '+player+' (muted: '+muted+')');
                        if (callback) {
                            callback();
                        }
                    };
                    kmsApps[player].app.listeners.onDispose = () => {
                        emitStopStreamLoading(player);
                        emitSelfStreamStopped(player);
                    };
                    kmsApps[player].app.presenter();

                    muted ? mute(player) : unmute(player);
                },
                kmsApps[player].stunServer
            );
            kmsApps[player].app.listeners.onError = () => emitStopStreamLoading(player);
        } catch (e) {
            emitStopStreamLoading(player);
        }
    }

    function startViewer(player, muted, callback) {
        if (kmsApps[player] && null !== kmsApps[player].app) {
            stop(player);
        }

        console.log('Viewer start on player: '+player);

        emitStartStreamLoading(player);

        window.setTimeout( () => {
            kmsApps[player].app = new KmsOneToMany(
                kmsApps[player].location,
                kmsApps[player].video,
                function () {
                    console.log('Viewer ready on player: '+player+' (muted: '+muted+')');

                    kmsApps[player].app.listeners.onViewerAccepted = () => {
                        console.log('Viewer accepted on player: '+player+' (muted: '+muted+')');
                        if (callback) {
                            callback();
                        }
                    };
                    kmsApps[player].app.listeners.onDispose = () => emitStopStreamLoading(player);
                    kmsApps[player].app.viewer();

                    muted ? mute(player) : unmute(player);
                },
                kmsApps[player].stunServer,
            );
            kmsApps[player].app.listeners.onError = () => emitStopStreamLoading(player);
        }, 3000);
    }

    function stop (player) {
        console.log('Stop player: '+player);

        emitSelfStreamStopped(player);

        if (kmsApps[player] && null !== kmsApps[player].app) {
            kmsApps[player].app.stop();
            kmsApps[player].app = null;
        }
    }

    function mute (player) {
        console.log('Mute player '+player);

        if (player && kmsApps[player] && null !== kmsApps[player].video) {
            kmsApps[player].video.muted = true;
        }
    }

    function unmute (player) {
        console.log('Unmute player '+player);

        if (player && kmsApps[player] && null !== kmsApps[player].video) {
            kmsApps[player].video.muted = false;
        }
    }

    function emitPlayerInitialized (player, playerState) {
        const e = new CustomEvent('onPlayerInitialized', {'detail': {target: player, playerState}});
        window.dispatchEvent(e);
    }

    function emitStreamStarted (player, userId) {
        const e = new CustomEvent('onStreamStarted', {'detail': {target: player, userId}});
        window.dispatchEvent(e);
    }

    function emitStreamStopped (player, userId) {
        const e = new CustomEvent('onStreamStopped', {'detail': {target: player, userId}});
        window.dispatchEvent(e);
    }

    function emitSelfStreamStarted (player) {
        const e = new CustomEvent('onSelfStreamStarted', {'detail': {target: player}});
        window.dispatchEvent(e);
    }

    function emitSelfStreamStopped (player) {
        const e = new CustomEvent('onSelfStreamStopped', {'detail': {target: player}});
        window.dispatchEvent(e);
    }

    function emitStartStreamLoading (player) {
        const e = new CustomEvent('onStreamLoadingStarted', {'detail': {video: kmsApps[player].video}});
        window.dispatchEvent(e);
    }

    function emitStopStreamLoading (player) {
        const e = new CustomEvent('onStreamLoadingStopped', {'detail': {video: kmsApps[player].video}});
        window.dispatchEvent(e);
    }

    // Public interface

    function init() {
        connectToServer();
    }

    return {
        init,
    }
}
