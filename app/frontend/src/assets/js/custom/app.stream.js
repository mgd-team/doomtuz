
const TARGETS = Object.freeze({
    PRESIDIUM: 'presidium',
    MEMBER: 'member',
    TRIBUNE: 'tribune',
    TABLE: 'tribune',
});

const streamBtns = Object.freeze({
    'member': {
        muteBtn: document.getElementById("mute-member-stream-btn"),
        unmuteBtn: document.getElementById("unmute-member-stream-btn"),
        stopBtn: document.getElementById("stop-member-stream-btn"),
        autoMute: true, // Should be muted when another speaker activated
    },
    'tribune': {
        muteBtn: document.getElementById("mute-tribune-stream-btn"),
        unmuteBtn: document.getElementById("unmute-tribune-stream-btn"),
        stopBtn: document.getElementById("stop-tribune-stream-btn"),
        autoMute: true,
    },
    'table': {
        muteBtn: document.getElementById("mute-table-stream-btn"),
        unmuteBtn: document.getElementById("unmute-table-stream-btn"),
        stopBtn: document.getElementById("stop-table-stream-btn"),
        autoMute: false,
    },
    'presidium': {
        muteBtn: document.getElementById("mute-presidium-stream-btn"),
        unmuteBtn: document.getElementById("unmute-presidium-stream-btn"),
        stopBtn: document.getElementById("stop-presidium-stream-btn"),
        autoMute: false,
    },
});
const $swapAudioBtns = $('.swap-audio-btn');

const muteStream = (target) => {
    if (!streamBtns[target].muteBtn.classList.contains('kt-hidden')) {
        const event = new CustomEvent('emit_onMuteStream', {detail: {target}});
        window.dispatchEvent(event);
    }
};

const unmuteStream = (target) => {
    if (!streamBtns[target].unmuteBtn.classList.contains('kt-hidden')) {
        const event = new CustomEvent('emit_onUnmuteStream', {detail: {target}});
        window.dispatchEvent(event);
    }
};

const muteStreamBtn = (key) => {
    if (streamBtns[key].muteBtn) {
        streamBtns[key].muteBtn.classList.add('kt-hidden');
        streamBtns[key].unmuteBtn.classList.remove('kt-hidden');
    }
};

const unmuteStreamBtn = (key) => {
    if (streamBtns[key].muteBtn) {
        streamBtns[key].muteBtn.classList.remove('kt-hidden');
        streamBtns[key].unmuteBtn.classList.add('kt-hidden');
    }
};

const stopStream = (key) => {
    const event = new CustomEvent('emit_onStopStream', {
        'detail': {
            target: key,
        }
    });
    window.dispatchEvent(event);
};

const startStream = (userId, key) => {
    const event = new CustomEvent('emit_onStartStream', {
        'detail': {
            userId,
            target: key,
        }
    });
    window.dispatchEvent(event);

    for (let target in streamBtns) {
        if (!streamBtns[target].autoMute) {
            continue;
        }

        if (key === target) {
            unmuteStream(target);
        } else {
            muteStream(target);
        }
    }
};

for (let key in streamBtns) {
    if (streamBtns[key].muteBtn) {
        streamBtns[key].muteBtn.onclick = () => {
            muteStream(key);
        };
    }
    if (streamBtns[key].unmuteBtn) {
        streamBtns[key].unmuteBtn.onclick = () => {
            unmuteStream(key);
        };
    }
    if (streamBtns[key].stopBtn) {
        streamBtns[key].stopBtn.onclick = () => {
            stopStream(key);
        };
    }

    const dropZone = document.getElementById(`${key}-drop-zone`);
    if (dropZone) {
        dropZone.ondragover = dropZone.ondragenter = function (event) {
            event.preventDefault();
            return false;
        };
        dropZone.ondrop = event => {
            const userId = event.dataTransfer.getData('text/plain');
            startStream(userId, key);

            event.preventDefault();
            return false;
        };
    }
}

$.each($swapAudioBtns, function (index, btn) {
    $(btn).on('click', function () {
        const leftTarget = $(btn).data('target-left');
        const rightTarget = $(btn).data('target-right');

        if (!streamBtns[leftTarget].muteBtn.classList.contains('kt-hidden')) {
            muteStream(leftTarget);
            unmuteStream(rightTarget);
        } else {
            unmuteStream(leftTarget);
            muteStream(rightTarget);
        }
    });
});

let originalVideoPoster = null;

function showSpinner(video) {
    if (null === originalVideoPoster) {
        originalVideoPoster = video.poster;
    }
    video.poster = '/assets/media/app/transparent-1px.png';
    video.style.background = 'center transparent url("/assets/media/app/spinner.gif") no-repeat';
}

function hideSpinner(video) {
    video.poster = originalVideoPoster;
    video.style.background = '';
}

function setSpeakerName(target, userId) {

    console.log(`Set speaker name: target: ${target}, userId: ${userId}`);

    if (TARGETS.MEMBER === target || TARGETS.TRIBUNE === target) {
        const speakerNameContainer = $(`#player-container-${target} .speaker-name`);
        speakerNameContainer.removeClass('kt-hidden');
        window.participants.forEach(element => {
            if (parseInt(element.id) === parseInt(userId)) {
                speakerNameContainer.html(element.name);
            }
        });
    }
}

var targetArr = [];
const showPlayers = (target) => {
    if(targetArr.indexOf(target) == -1){
        targetArr.push(target)
    }
    let container = document.getElementById("player-container-"+target),
        containerParent = container.parentElement,
        player = document.getElementById(target+"-player"),
        allContainers = document.getElementById("player-containers").getElementsByClassName('kt-user'),
        allPlayers = document.getElementById("player-containers").getElementsByClassName('player')

    if (targetArr.length === 1){
        Array.from(allContainers).forEach((container)=>{
            container.classList.add("kt-hidden")
            container.classList.remove("col-md-12")
        })
        containerParent.classList.add("col-md-12")
        containerParent.classList.remove("kt-hidden")
        player.classList.add("one-player")
    } else {
        if(targetArr.length === 2) {
            player.classList.add("one-player")
        } else {
            Array.from(allPlayers).forEach((player) => {
                player.classList.remove("one-player")
            })
        }
        Array.from(allContainers).forEach((container)=>{
            container.classList.remove("col-md-12")
        })
        containerParent.classList.remove("kt-hidden")
    }
}

const hidePlayer = (target) => {
    targetArr = targetArr.filter((item) => {
        if(item !== target){
            return item
        }
    })
    let container = document.getElementById("player-container-"+target),
        player = document.getElementById(target+"-player"),
        containerParent = container.parentElement
    containerParent.classList.add("col-md-12")
    player.classList.add("one-player")
    if(targetArr.length > 0) {
        containerParent.classList.add("kt-hidden")
    }
    if(targetArr.length === 2){
        targetArr.map((target) => {
            let playerTarget = document.getElementById(target+"-player")
            playerTarget.classList.add("one-player")
        })
    }
}

window.addEventListener('onPlayerInitialized', function (event) {
    const { detail: { target: target, playerState: playerState } } = event;

    console.log('onPlayerInitialized event received:', event);
    if ('user' === window.ifaceMode) {
        showPlayers(target)
    }
    setSpeakerName(target, playerState.userId);

    if (playerState.muted) {
        muteStreamBtn(target);
    }
});

window.addEventListener('onStreamStarted', function (event) {
    const { detail: { target: target, userId: userId } } = event;

    console.log('onStreamStarted event received:', event);

    setSpeakerName(target, userId);
});

window.addEventListener('onStreamStopped', function (event) {
    const { detail: { target: target } } = event;

    if ('user' === window.ifaceMode){
        hidePlayer(target)
    }
    console.log('onStreamStopped event received:', event);

    const speakerNameContainer = $(`#player-container-${target} .speaker-name`);
    speakerNameContainer.addClass('kt-hidden');
    speakerNameContainer.html('');
});

window.addEventListener('onSelfStreamStarted', function (event) {
    const { detail: { target: target } } = event;

    console.log('onSelfStreamStarted event received:', event);

    if ('user' !== ifaceMode) {
        return;
    }
    showPlayers(target)
    if (TARGETS.PRESIDIUM === target) {
        const buttonsContainer = $(`#player-container-${target} .buttons-container`);
        buttonsContainer.removeClass('kt-hidden');
        buttonsContainer.find('[class*=-btn-container]').hide();
        buttonsContainer.find('.mute-btn-container').show();
    }
});

window.addEventListener('onSelfStreamStopped', function (event) {
    const { detail: { target: target } } = event;

    console.log('onSelfStreamStopped event received:', event);

    if ('user' !== ifaceMode) {
        return;
    }
    showPlayers(target)
    if (TARGETS.PRESIDIUM === target) {
        $(`#player-container-${target} .buttons-container`).addClass('kt-hidden');
    }
});

window.addEventListener('onStreamLoadingStarted', function (event) {
    const { detail: { video: video } } = event;

    console.log('onStreamLoadingStarted event received:', event);

    showSpinner(video);
});

window.addEventListener('onStreamLoadingStopped', function (event) {
    const { detail: { video: video } } = event;

    console.log('onStreamLoadingStopped event received:', event);

    hideSpinner(video);
});

window.addEventListener('onMuteStream', function (event) {
    const { detail: { target: target } } = event;
    // targetArr = targetArr.filter((item) => {
    //     item !== target
    // })
    // hidePlayer(target)
    muteStreamBtn(target);
});

window.addEventListener('onUnmuteStream', function (event) {
    const { detail: { target: target } } = event;
    unmuteStreamBtn(target);
});
