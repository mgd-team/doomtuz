<?php

namespace App\Entity;

class User extends \MGD\AuthBundle\Model\User
{
    const ROLES = [
        1 => self::ROLE_PARTICIPANT,
        2 => self::ROLE_DEPUTY,
        4 => self::ROLE_ADMIN,
        5 => self::ROLE_SUPER_ADMIN,
    ];

    const ROLE_PARTICIPANT = 'ROLE_PARTICIPANT';
    const ROLE_DEPUTY = 'ROLE_DEPUTY';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    private int $registrationId;

    /**
     * @return int|null
     */
    public function getRegistrationId()
    {
        return $this->registrationId;
    }

    public function setRegistrationId(int $registrationId): self
    {
        $this->registrationId = $registrationId;

        return $this;
    }
}
