<?php

namespace App\Security;

use App\Entity\User;
use MGD\AuthBundle\AuthClient\MgdAuthClient;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class UserProvider implements UserProviderInterface
{
    /**
     * @var MgdAuthClient
     */
    private $mgdAuth;

    /**
     * @var CacheInterface
     */
    private $cache;

    public function __construct(MgdAuthClient $mgdAuth, CacheInterface $cache)
    {
        $this->mgdAuth = $mgdAuth;
        $this->cache = $cache;
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        /** @var UserInterface[] $users */
        $users = $this->cache->get('users', function (ItemInterface $item) {
            $item->expiresAfter(3600);

            return $this->mgdAuth->getUsers();
        });

        foreach ($users as $user) {
            if (!strcasecmp(mb_strtolower($user->getUsername()), mb_strtolower($username))) {
                return $user;
            }
        }

        throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        return $user;
    }

    public function supportsClass(string $class): bool
    {
        return User::class === $class;
    }
}
