<?php

namespace App\Security;

use App\Entity\User;
use MGD\APIBundle\Client\Client;
use MGD\AuthBundle\AuthClient\MgdAuthClient;
use MGD\AuthBundle\Security\MgdAuthenticator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;

class Authenticator extends MgdAuthenticator
{
    private Client $apiService;

    private RouterInterface $router;

    public function __construct(
        MgdAuthClient $authService,
        Client $apiService,
        RouterInterface $router
    ) {
        parent::__construct($authService);
        $this->apiService = $apiService;
        $this->router = $router;
    }

    public function supports(Request $request): bool
    {
        return parent::supports($request) && $request->get('_registration');
    }

    public function getCredentials(Request $request): array
    {
        return [
            'username' => $request->get('_username'),
            'password' => $request->get('_password'),
            'registration' => $request->get('_registration'),
        ];
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new \LogicException('User must be instance of '.User::class.' class');
        }
        if (!parent::checkCredentials($credentials, $user)) {
            return false;
        }

        $data = $this->apiService->post(
            'api/registration-for-session/register/'.$user->getSuiId().'/'.$credentials['registration'],
            [
                'token' => $this->apiService->getToken(),
                'ipUser' => empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'],
            ]
        );

        $user->setRegistrationId($credentials['registration']);

        $user->setRoles([]);

        $roleMask = $data->response->role;
        foreach (array_keys(User::ROLES) as $role) {
            if ($role === $roleMask) {
                $user->addRole(User::ROLES[$role]);
            }
        }

        return !empty($user->getRoles());
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey): RedirectResponse
    {
        if (!($user = $token->getUser()) instanceof User) {
            throw new \LogicException('User must be instance of '.User::class.' class');
        }

        return new RedirectResponse($this->router->generate('index'));
    }

    /**
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return null;
    }

    /**
     * Called when authentication is needed, but it's not sent.
     */
    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse('/login');
    }
}
