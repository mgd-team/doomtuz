<?php

namespace App\Model;

use JMS\Serializer\Annotation\Type;

class Participant
{
    const ROLE_NONE = 0; // без роли
    const ROLE_PARTICIPANT = 1; // участник
    const ROLE_DEPUTY = 2; // депутат
    const ROLE_ADMIN = 4; // администратор
    const ROLE_SUPER_ADMIN = 5; // супер администратор

    /**
     * @Type("int")
     */
    private int $id;

    /**
     * @Type("string")
     */
    private string $name;

    /**
     * @Type("string")
     */
    private ?string $avatar;

    /**
     * @Type("int")
     */
    private int $role;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function getRole(): int
    {
        return $this->role;
    }
}
