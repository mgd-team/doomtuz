<?php

namespace App\Model;

use JMS\Serializer\Annotation\Type;

class Translation
{
    /**
     * @Type("int")
     */
    private int $id;

    /**
     * @Type("array<App\Model\Question>")
     */
    private array $questions;

    public function getId(): int
    {
        return $this->id;
    }

    public function getQuestions(): array
    {
        return $this->questions;
    }
}
