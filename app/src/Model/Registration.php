<?php

namespace App\Model;

use JMS\Serializer\Annotation\Type;

class Registration
{
    /**
     * @Type("int")
     */
    private int $id;

    /**
     * @Type("App\Model\Translation")
     */
    private Translation $translation;

    /**
     * @Type("string")
     */
    private string $name;

    /**
     * @Type("array<App\Model\Participant>")
     */
    private array $participants;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Participant[]|array
     */
    public function getParticipants(): array
    {
        return $this->participants;
    }

    public function getTranslation(): Translation
    {
        return $this->translation;
    }
}
