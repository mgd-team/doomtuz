<?php

namespace App\Model;

use JMS\Serializer\Annotation\Type;

class Question
{
    /**
     * @Type("int")
     */
    private int $id;

    /**
     * @Type("string")
     */
    private string $question;

    public function getId(): int
    {
        return $this->id;
    }

    public function getQuestion(): string
    {
        return $this->question;
    }
}
