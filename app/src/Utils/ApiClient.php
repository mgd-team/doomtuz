<?php

namespace App\Utils;

use App\Model\Registration;
use JMS\Serializer\Serializer;
use MGD\APIBundle\Client\Client;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class ApiClient
{
    private const CONFIGURE_URL = '/api/registration-for-session/configure';
    const VOTING_RESULTS_LIST_URL = '/api/registration-for-session/get-upcoming-conference-voting';
    const VOTING_DETAILS_BY_ID_URL = '/api/registration-for-session/get-voting-details?conference_id=%s&voting_id=%s';
    const SET_REMOTE_TRANSLATION_VOTING = '/api/registration-for-session/set-voting-remote-translation';
    const GET_REMOTE_TRANSLATION_VOTING = '/api/registration-for-session/get-voting-remote-translation';
    const GET_SESSION_QUESTIONS_ID = '/api/registration-for-session/get-session-questions-id';
    const GET_REMOTE_TRANSLATION_EMPOLYEE = '/api/registration-for-session/get-employee-remote-translation';
    const GET_QUESTION_FILES = '/api/registration-for-session/get-question-files';

    private Client $client;
    private ?array $configurations = null;
    private Serializer $serializer;
    /**
     * @var CacheInterface
     */
    private $cache;

    public function __construct(Client $client, Serializer $serializer, CacheInterface $cache)
    {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->cache = $cache;
    }

    private function configure(): void
    {
        if (null !== $this->configurations) {
            return;
        }

        $data = $this->client->post(self::CONFIGURE_URL);

        $this->configurations = $data->response;
    }

    /**
     * @return Registration[]|array
     */
    public function getRegistrations(): array
    {
        $this->configure();

        $registrations = [];
        if ($this->configurations) {
            foreach ($this->configurations as $configuration) {
                if (null === $configuration ||
                    !property_exists($configuration, 'registration') ||
                    !property_exists($configuration, 'translation')
                ) {
                    throw new \RuntimeException('Invalid response data');
                }

                $configuration->registration->translation = $configuration->translation;

                $registrations[] = $this->serializer->deserialize(
                    (string) json_encode($configuration->registration),
                    Registration::class,
                    'json'
                );
            }
        }

        return $registrations;
    }

    /**
     * @return mixed
     */
    public function getRemoteTranslationVotingResults(array $params)
    {
        return $this->cache->get(sprintf('remote_translation_voting_results'), function (ItemInterface $item) use ($params) {
            $item->expiresAfter(60);
            $data = $this->client->post(sprintf(self::GET_REMOTE_TRANSLATION_VOTING), $params);

            return json_decode(json_encode($data->response) ?: '', true);
        });
    }

    /**
     * @return mixed
     */
    public function getSessionQuestionsId(array $params)
    {
        $data = $this->client->post(sprintf(self::GET_SESSION_QUESTIONS_ID), $params);

        return json_decode(json_encode($data->response) ?: '', true);
    }

    /**
     * @return mixed
     */
    public function getVotingResults()
    {
        return $this->cache->get('voting_results', function (ItemInterface $item) {
            $item->expiresAfter(60);
            $data = $this->client->post(self::VOTING_RESULTS_LIST_URL);

            return json_decode(json_encode($data->response) ?: '', true);
        });
    }

    /**
     * @return mixed
     */
    public function getVotingDetailsById(array $params)
    {
        return $this->cache->get(sprintf('voting_results_%s', $params['voting_id']), function (ItemInterface $item) use ($params) {
            $item->expiresAfter(360);
            $data = $this->client->post(sprintf(self::VOTING_DETAILS_BY_ID_URL, $params['conference_id'], $params['voting_id']));

            return json_decode(json_encode($data->response) ?: '', true);
        });
    }

    /**
     * @return mixed
     */
    public function setRemoteTranslationVoting(array $params)
    {
        $result = $this->client->post(self::SET_REMOTE_TRANSLATION_VOTING, ['params' => $params]);

        return json_decode(json_encode($result->response) ?: '', true);
    }

    /**
     * @return mixed
     */
    public function getRemoteTranslationVoting()
    {
        $data = $this->client->post(self::GET_REMOTE_TRANSLATION_VOTING);

        return json_decode(json_encode($data->response) ?: '', true);
    }

    /**
     * @return mixed
     */
    public function getRemoteTranslationEmployee(string $params)
    {
        $data = $this->client->post(self::GET_REMOTE_TRANSLATION_EMPOLYEE, ['params' => $params]);

        return json_decode(json_encode($data->response) ?: '', true);
    }

    /**
     * @return mixed
     */
    public function getQuestionFiles(string $id)
    {
        $data = $this->client->post(self::GET_QUESTION_FILES, ['questionId' => $id]);

        return json_decode(json_encode($data->response) ?: '', true);
    }
}
