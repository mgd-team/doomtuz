
var offlinePage = new Request('/offline.html');
self.addEventListener('install', function(event) {

    event.waitUntil(
        fetch(offlinePage).then(function(response) {
            return caches.open('pwabuilder-offline').then(function(cache) {
                console.log('[PWA Builder] Cached index page during Install'+ response.url);
                return cache.put(offlinePage, response);
            });
        }));
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        fetch(event.request).catch(function(error) {
            console.log( '[PWA Builder] Network request Failed. Serving content from cache: ' + error );
            return offlinePage;
        })
    );
});
