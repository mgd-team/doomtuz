const express = require("express");
const app = express();
const fs = require("fs");

const SERVER_PORT = 3001;
const targets = {};

app.use(express.static("static"));
const server = require("https").createServer(
    {
      key: fs.readFileSync("ssl/selfsigned.key"),
      cert: fs.readFileSync("ssl/selfsigned.crt")
    },
    app
);

const io = require("socket.io")(server, { forceNew: true });
io.set(server, ['websocket']);

io.on("connection", function(socket) {
  socket.emit("message", "Hello from Stream Router Socket Server!");
  console.log("User connected");

  let userId = null;

  socket.on("init", function(event) {
    console.log(`Init process ${event.userId}`);
    userId = event.userId;

    socket.emit("ready", targets);
  });

  socket.on("disconnect", function() {
    console.log(`User ${userId} disconnected`);
    console.info('Disconnect: Before disconnect targets: ', targets);

    io.sockets.emit("onStreamStopped", {
      userId,
      target: getTarget(userId)
    });

    console.info('Disconnect: Current targets: ', targets);
  });

  socket.on("onStartStream", function(event) {
    console.log(`User ${event.userId} should start stream on ${event.target}`);
    io.sockets.emit("onStartStream", event);
  });

  socket.on("onStreamStarted", function(event) {
    console.log(`User ${event.userId} have started stream on ${event.target}`);
    io.sockets.emit("onStreamStarted", event);
    addTarget(event.userId, event.target);
  });

  socket.on("onStopStream", function(event) {
    if (!event.userId) {
      event.userId = getUserId(event.target);
    }
    console.log(`User ${event.userId} should stop stream on ${event.target}`);
    io.sockets.emit("onStopStream", event);
  });

  socket.on("onStreamStopped", function(event) {
    console.log(`User ${event.userId} have stopped stream on ${event.target}`);
    io.sockets.emit("onStreamStopped", event);
    removeTarget(event.userId, event.target);
  });

  socket.on("onMuteStream", function(event) {
    if (!event.userId) {
      event.userId = getUserId(event.target);
    }
    console.log(`User ${event.userId} should mute stream on ${event.target}`);
    muteTarget(event.userId, event.target);
    io.sockets.emit("onMuteStream", event);
  });

  socket.on("onUnmuteStream", function(event) {
    if (!event.userId) {
      event.userId = getUserId(event.target);
    }
    console.log(`User ${event.userId} should unmute stream on ${event.target}`);
    unmuteTarget(event.userId, event.target);
    io.sockets.emit("onUnmuteStream", event);
  });

  socket.on("error", function(e) {
    console.log("socket.io error:" + e);
  });

  socket.on("broadcast", function(event) {
    console.log(`Broadcast message from ${userId}: `, event);
    io.sockets.emit("broadcast", event);
  });
});

io.on("error", function(e) {
  console.log("socket.io error:" + e);
});

const addTarget = function (userId, target) {
  targets[target] = {
    userId,
    target,
    muted: false,
    startedAt: (new Date()).getTime(),
  };
};

const removeTarget = function (userId, target) {
  if (targets[target] && targets[target].userId === userId) {
    delete targets[target];
  }
};

const muteTarget = function (userId, target) {
  if (targets[target] && targets[target].userId === userId) {
    targets[target].muted = true;
  }
};

const unmuteTarget = function (userId, target) {
  if (targets[target] && targets[target].userId === userId) {
    targets[target].muted = false;
  }
};

const getTarget = function (userId) {
  for (let target in targets) {
    if (targets[target].userId === userId) {
      return target;
    }
  }

  return null;
};

const getUserId = function (neededTarget) {
  for (let target in targets) {
    if (target === neededTarget) {
      return targets[target].userId;
    }
  }

  return null;
};

server.listen(SERVER_PORT, function() {
  console.log("https and websocket listening on *:3001");
});

process.on("uncaughtException", function(err) {
  // handle the error safely
  console.log(err);
});
