ТУЗ
========================

Prerequisites
-------------
To build project and start development on host machine must be installed Linux (recommended) or MacOS and some required software:
latest version of `Docker (version 18.06.0+)` [https://docs.docker.com/install/] 
and `docker-compose` [https://docs.docker.com/compose/compose-file/], 
`Git`, `Java 8` (to run Gradle tasks) and `PHP 7.1+` (to run code style checker before commit).  

To build frontend you should install `npm` and `webpack`.

Installation
-------------

You should run commands below to install application correctly:
#Without "sudo" will not be setted vm.max_map_count = 262144
# sudo sysctl -w vm.max_map_count=262144

        sudo ./gradlew build

To rebuild frontend run in app container:

        cd frontend/tools
        gulp
        
Next you can check services status:

        docker-compose ps

Default port mapping (see `.env` file):

        127.0.0.1:8080 - App service
