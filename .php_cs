<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__.'/app/src')
    ->in(__DIR__.'/app/tests')
;

return PhpCsFixer\Config::create()
    ->setRules([
        '@Symfony' => true,
    ])
    ->setFinder($finder)
;
